#define STB_IMAGE_IMPLEMENTATION

extern "C"
{
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <stb/stb_image.h>
}

#include <iostream>
#include <fstream>
#include <any>
#include <cmath>
#include <thread>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace app
{
  GLFWwindow *window;
  float tex_coords[] =
    {
      0.0f, 0.0f,  // lower-left corner  
      1.0f, 0.0f,  // lower-right corner
      0.5f, 1.0f   // top-center corner
    },
    boarder_colour[] =
    {
      1.0f, 1.0f, 0.0f, 1.0f
    };
  
  inline
  void process_input ()
  {
    if (glfwGetKey (window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
      {
	glfwSetWindowShouldClose (window, true);
      }
  }

  inline std::string
  load_file (std::string file)
  {
    std::ifstream in (file);
    std::string  file_content;
    if (in.is_open ())
      {   
	while (!in.eof ())
	  {
	    std::string s;
	    getline (in, s);
	    file_content += s + "\n";
	    
	  }
      }
    in.close ();
    return file_content;
  }
}

int
main ()
{
  // Create GLFW window:
  if (!glfwInit ())
    {
      std::cerr << "GLFW failed to initalise!" << std::endl;
      exit (EXIT_FAILURE);
    }

  GLFWmonitor* monitor = glfwGetPrimaryMonitor();
  const GLFWvidmode* mode = glfwGetVideoMode(monitor);
  glfwWindowHint(GLFW_RED_BITS, mode->redBits);
  glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
  glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
  glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);

  app::window = glfwCreateWindow (mode->width, mode->height, PACKAGE, monitor, NULL);
  if (!app::window)
    {
      glfwTerminate ();
      std::cerr << "Failed to create window!" << std::endl;
      exit (EXIT_FAILURE);
    }
  
  glfwMakeContextCurrent(app::window);
  glewInit (); // Needed to avoid segmentation fault.

  glfwSetInputMode(app::window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
  glfwSwapInterval(0);

  // Square:
  glClearColor(0.3f, 0.0f, 0.2f, 1.0f);
  
  float vertices[] =
    {
      // Positions:          // Colors:           // Texture coords:
      0.5f,  0.5f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 1.0f,   // top right
      0.5f, -0.5f, 0.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f,   // bottom right
      -0.5f, -0.5f, 0.0f,  0.0f, 0.0f, 1.0f,   0.0f, 0.0f,   // bottom left
      -0.5f,  0.5f, 0.0f,  1.0f, 1.0f, 0.0f,   0.0f, 1.0f    // top left 
    };

  // Vertex shader:
  std::string  vertex_shader_src = app::load_file (SHADER_DIR "/shader.vert");
  const char *c_vertex_str = vertex_shader_src.c_str();
  unsigned int vertex_shader = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource (vertex_shader, 1, &c_vertex_str, nullptr);
  glCompileShader (vertex_shader);
  int success;
  glGetShaderiv (vertex_shader, GL_COMPILE_STATUS, &success);  
  if (!success)
    {
      char info_log[512];
      glGetShaderInfoLog (vertex_shader, sizeof (info_log), nullptr, info_log);
      std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << info_log << "\n";	  
    }

  // Fragment shader:
  std::string fragment_shader_src = app::load_file (SHADER_DIR "/shader.frag");
  const char *c_fragment_str = fragment_shader_src.c_str ();
  unsigned int fragment_shader = glCreateShader (GL_FRAGMENT_SHADER);
  glShaderSource (fragment_shader, 1, &c_fragment_str, nullptr);
  glCompileShader (fragment_shader);
  glGetShaderiv (fragment_shader, GL_COMPILE_STATUS, &success);
  if (!success)
    {
      char info_log[512];
      glGetShaderInfoLog (vertex_shader, sizeof (info_log), nullptr, info_log);
      std::cout << "ERROR::SHADER:FRAGMENT::COMPILATION_FAILED\n" << info_log << "\n";
    }

  unsigned int shader_program = glCreateProgram ();
  glAttachShader (shader_program, vertex_shader);
  glAttachShader (shader_program, fragment_shader);
  glLinkProgram (shader_program);

  glGetProgramiv (shader_program, GL_LINK_STATUS, &success);
  if (!success)
    {
      char info_log[512];
      glGetProgramInfoLog (shader_program, sizeof (info_log), nullptr, info_log);
    }


  unsigned int
    indices[] =
    {
      0, 1, 3, // first triangle
      1, 2, 3  // second triangle
    },
    vbo,
    vao,
    ebo;

  glGenVertexArrays(1, &vao);
  glGenBuffers (1, &vbo);
  glGenBuffers (1, &ebo);

  glBindVertexArray(vao);
  glBindBuffer (GL_ARRAY_BUFFER, vbo);
  glBufferData (GL_ARRAY_BUFFER, sizeof (vertices), vertices, GL_STATIC_DRAW);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof (indices), indices, GL_STATIC_DRAW);

  // Position attribute:
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof (float), static_cast<void *> (0));
  glEnableVertexAttribArray(0);				     
  // Colour attribute:					     
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof (float), reinterpret_cast<void *> (3 * sizeof (float)));
  glEnableVertexAttribArray(1);				     
  // Texture coord attribute:				     
  glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof (float), reinterpret_cast<void *> (6 * sizeof (float)));
  glEnableVertexAttribArray(2);

  // Texture:
  unsigned int texture;
  glGenTextures (1, &texture);
  glBindTexture(GL_TEXTURE_2D, texture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  int width, height, nr_channels;
  stbi_set_flip_vertically_on_load(true);
  unsigned char *data = stbi_load(TEXTURE_DIR "/gnu.png", &width, &height, &nr_channels, 0);
  if (!data)
    {
      std::cout << "Failed to load texture!\n";
    }
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
  glGenerateMipmap(GL_TEXTURE_2D);
  stbi_image_free(data);

  glm::mat4 trans;
  unsigned int transformLoc;
  
  std::jthread input_thread ([&] ()
  {
    while (!glfwWindowShouldClose (app::window))
      {
	app::process_input ();
      }
  });
  
  glUseProgram (shader_program);

  glBindTexture (GL_TEXTURE_2D, texture);
  
  /* Render loop. */
  while (!glfwWindowShouldClose (app::window))
    { 
      glClear(GL_COLOR_BUFFER_BIT);

      trans = glm::mat4(1.0f);
      trans = glm::scale(trans, glm::vec3(0.75, 1.0, 1.0));
      trans = glm::rotate(trans, static_cast<float> (glfwGetTime ()), glm::vec3(0.0, 0.0, 1.0));
      transformLoc = glGetUniformLocation(shader_program, "transform");
      glUniformMatrix4fv(transformLoc, 1, GL_FALSE, glm::value_ptr(trans));

      glBindVertexArray(vao);
      glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
      
      glfwSwapBuffers (app::window);
      glfwPollEvents ();
    }

  glfwTerminate ();
  glDeleteShader (vertex_shader);
  glDeleteShader (fragment_shader);
}
